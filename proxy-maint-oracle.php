<?php
    session_start();

    $conf['db_user']   = '***username***';
    $conf['db_pass']   = '***password***';
    $conf['db_host']   = '***sid***';
    $conf['db_schema'] = '***schema***';

    $paramStart    = $_GET['start']             ? $_GET['start']              : 0;
    $paramLimit    = $_GET['limit']             ? $_GET['limit']              : 200;
    if($_GET['sort']) {
	$arr = json_decode(stripslashes($_GET['sort']), true);
	$paramSort = $arr[0]['property'];
	$paramDir  = $arr[0]['direction'];
    } else {
	$paramSort = '';
	$paramDir  = 'ASC';
    }
    $paramAction   = $_SERVER['REQUEST_METHOD'] ? $_SERVER['REQUEST_METHOD']  : 'GET';
    $paramSearch   = $_GET['search']            ? strtolower($_GET['search']) : '';
    $paramSFields  = $_GET['sfields']           ? $_GET['sfields']            : '';
    list($paramTable,$paramId) = explode('/', trim($_SERVER['PATH_INFO'], '/'));

    $conn = dbConnect();
    switch ($paramAction) {
	case 'GET':
	    $stid = oci_parse($conn, getSelectTotalSQL($paramTable, getSelectSearchSQL($paramSearch, $paramSFields)));
	    oci_define_by_name($stid, 'NUM_TOTAL', $total);
	    oci_execute($stid);
	    oci_fetch($stid);
	    oci_free_statement($stid);
	    $stid = oci_parse($conn, getSelectRowsSQL($paramTable, $paramSort, $paramDir, getSelectSearchSQL($paramSearch, $paramSFields)));
	    oci_execute($stid);
	    oci_fetch_all($stid, $records, $paramStart, $paramLimit, OCI_FETCHSTATEMENT_BY_ROW + OCI_ASSOC);
	    oci_free_statement($stid);
	    print json_encode(array(success => true, total => $total, records => $records));
	    break;
	case 'POST':
	    $json      = file_get_contents('php://input');
	    $json      = preg_replace('/\:true/',  ':"1"', $json);
	    $json      = preg_replace('/\:false/', ':"0"', $json);
	    $arr       = (array)json_decode($json);
	    $post      = (array)$arr['records'];
	    $paramPKey = $_GET['pkey'];
	    $paramSeq  = $_GET['seq'];
	    if ($paramSeq) {
		$stid = oci_parse($conn, getInsertSequenceSQL($paramSeq));
		oci_define_by_name($stid, 'NUM_SEQVAL', $paramSeqVal);
		oci_execute($stid);
		oci_fetch($stid);
		oci_free_statement($stid);
		$post[$paramPKey] = $paramSeqVal;
	    }
	    $sql_field_arr = array();
	    $sql_value_arr = array();
	    $i = 1;
	    foreach($post as $field => $value) {
		$sql_field_arr[] = $field;
		$sql_value_arr[] = ":VAL$i";
		$i++;
		if (substr($field, 0, 3) == 'DT_') {
		    $post[$field] = preg_replace('/T/', ' ', $value);
		}
	    }
	    $sql_field = join(',', $sql_field_arr);
	    $sql_value = join(',', $sql_value_arr);
	    $sql = getInsertRowSQL($paramTable, $sql_field, $sql_value);
	    $stid = oci_parse($conn, $sql);
	    $i = 1;
	    foreach($post as $field => $value) {
		oci_bind_by_name($stid, ":VAL$i", $post[$field]);
		$i++;
	    }
	    oci_execute($stid);
	    oci_free_statement($stid);
	    oci_commit($conn);
	    $records[0] = $post;
	    print json_encode(array(success => true, sql => $sql, records => $records));
	    break;
	case 'PUT':
	    $json      = file_get_contents('php://input');
	    $json      = preg_replace('/\:true/',  ':"1"', $json);
	    $json      = preg_replace('/\:false/', ':"0"', $json);
	    $arr       = (array)json_decode($json);
	    $_PUT      = (array)$arr['records'];
	    $paramPKey = getPrimaryKeys();
	    foreach($_PUT as $field => $value) {
		if (substr($field, 0, 3) == 'DT_') {
		    $value = preg_replace('/T/', ' ', $value);
		}
		if ($field != $paramPKey) {
		    $sql = getUpdateSQL($paramTable, $field, $paramPKey);
		    $stid = oci_parse($conn, $sql);
		    oci_bind_by_name($stid, ':VALUE', $value);
		    for($i=0;$i<count($paramPKey);$i++){
			oci_bind_by_name($stid, ':ID'.$i, $_PUT[$paramPKey[$i]]);
		    }
		    if(!oci_execute($stid)){
			$e = oci_error($conn);
			$errorMessage = htmlentities($e['message']);
		    }
		    oci_free_statement($stid);
		}
	    }
	    oci_commit($conn);
	    if($errorMessage){
		print json_encode(array('success' => false,'message' => $errorMessage));
	    } else {
		print json_encode(array('success' => true));
	    }
	    break;
	case 'DELETE':
	    $json      = file_get_contents('php://input');
	    $arr       = (array)json_decode($json);
	    $paramPKey = $_GET['pkey'];
	    $parr      = get_object_vars($arr['records']);
	    $id        = $parr[$paramPKey];
	    $sql       = getDeleteSQL($paramTable, $paramPKey);
	    $stid      = oci_parse($conn, $sql);
	    oci_bind_by_name($stid, ':ID', $id);
	    oci_execute($stid);
	    oci_free_statement($stid);
	    print json_encode(array(success => true, sql => $sql, id => $id));
	    break;
	default:
	    print json_encode(array(success => false));
    }
    dbDisconnect($conn);


    function dbConnect() {
	global $conf;
	$conn = oci_connect($conf['db_user'], $conf['db_pass'], $conf['db_host'], 'AL32UTF8');
	if (!$conn) {
	    $err = oci_error();
	    trigger_error(htmlentities($err['message'], ENT_QUOTES), E_USER_ERROR);
	}
	$stid = oci_parse($conn, "ALTER SESSION SET NLS_DATE_FORMAT = 'yyyy-mm-dd hh24:mi:ss'");
	oci_execute($stid);
	oci_free_statement($stid);
	$stid = oci_parse($conn, "ALTER SESSION SET NLS_NUMERIC_CHARACTERS = '.,'");
	oci_execute($stid);
	oci_free_statement($stid);
	return $conn;
    }

    function dbDisconnect($conn) {
	return oci_close($conn);
    }

    function getSelectRowsSQL($table, $sort, $dir, $where) {
	global $conf;
	$sql_rows = sprintf("SELECT * FROM %s.%s", $conf['db_schema'], $table);
	if ($where) {
	    $sql_rows .= sprintf(" WHERE %s", $where);
	}
	if ($sort) {
	    $sql_rows .= sprintf(" ORDER BY %s %s", $sort, $dir);
	}
        return $sql_rows;
    }

    function getSelectTotalSQL($table, $where) {
	global $conf;
	$sql_total = sprintf("SELECT COUNT(*) num_total FROM %s.%s", $conf['db_schema'], $table);
	if ($where) {
	    $sql_total .= sprintf(" WHERE %s", $where);
	}
	return $sql_total;
    }

    function getSelectSearchSQL($search, $sfields) {
	global $conf;
	$sql_where = '';
	if($search) {
	    if($sfields) {
		$fields = explode(',', $sfields);
		foreach($fields as $field) {
		    if ($field == 'num_user_department') {
			$sql_where .= sprintf("OR %s IN (SELECT num_user_department FROM %s.user_department WHERE LOWER(txt_user_department) LIKE '%%%s%%') ",
			    $field, $conf['db_schema'], $search);
		    } else {
			$sql_where .= sprintf("OR LOWER(%s) LIKE '%%%s%%' ", $field, $search);
		    }
		}
	    }
	    return '('.substr($sql_where, 3).')';
	} else {
	    return '';
	}
    }

    function getInsertRowSQL($table, $fields, $values) {
	global $conf;
	$sql = sprintf("INSERT INTO %s.%s(%s) VALUES(%s)", $conf['db_schema'], $table, $fields, $values);
	return $sql;
    }

    function getInsertSequenceSQL($sequence) {
	global $conf;
	$sql = sprintf("SELECT %s.%s.NEXTVAL num_seqval FROM dual", $conf['db_schema'], $sequence);
	return $sql;
    }

    function getUpdateSQL($table, $field, $pkey) {
	global $conf;
	$whereStr = "";
	for($i=0;$i<count($pkey);$i++){
	    if($i) $whereStr .= " AND ";
	    $whereStr .= $pkey[$i]." = :ID$i ";
	}
	return sprintf("UPDATE %s.%s SET %s = :VALUE WHERE $whereStr", $conf['db_schema'], $table, $field);
    }

    function getDeleteSQL($table, $pkey) {
	global $conf;
	return sprintf("DELETE FROM %s.%s WHERE %s = :ID", $conf['db_schema'], $table, $pkey);
    }

    function getPrimaryKeys(){
	$retArr = array();
	$tmpArr = array_filter(array_keys($_GET),function($key){return (strpos($key,'pkey')!== false);});
	foreach($tmpArr as $key){
	    $retArr[] = $_GET[$key];
	}
	return $retArr;
    }
?>